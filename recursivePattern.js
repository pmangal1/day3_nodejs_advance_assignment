// var somelist = readVeryLongList();
var somelist = [1, 2, 3, 4];
var nextItem = function () {
  var item = somelist.pop();
  if (item) {
    console.log(item);
    Promise.resolve(nextItem()); // pushed into microtask queue
  }
};

