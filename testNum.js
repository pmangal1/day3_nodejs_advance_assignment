const testNum = (n) => {
  return new Promise((resolve, reject) => {
    if (n >= 10) resolve('Value greater than equal to 10');
    else reject(new Error('Value is less than 10'));
    // else reject(new Error'Value is less than 10');
  });

};

testNum(20)
.then(res => console.log(res))
.catch(err => console.log(err))

