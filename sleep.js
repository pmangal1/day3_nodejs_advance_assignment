const sleep = (waitingTime, callback) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(callback) // callback returned
    }, waitingTime)
  })
}

const showText = () => console.log('WOW') // callback function

sleep(3000, showText)
  .then(callback => {
    callback() // calling callback after particular time
  })
  .catch(err => console.log(err))

