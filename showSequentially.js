const showSequentially = (number) => {
  promise = Promise.resolve(); // first promise resolved
  for (let i = 0; i <= number; i++) {
    // promise object returned for next process
    promise = promise.then(res => {
      return new Promise((resolve, reject) => {
        let randomeTime = Math.floor(Math.random() * 7) * 1000; // random time between 0-6 sec
        setTimeout(() => {
          console.log(i, randomeTime);
          resolve();
        }, randomeTime);
      })

    })
  }
}

showSequentially(10);

