// capitalize strings
const makeAllCaps = (words) => {
  return new Promise((resolve, reject) => {
    for (let i = 0; i < words.length; i++) {
      if (typeof words[i] === 'string') {
        words[i] = words[i].toUpperCase()
      } else {
        reject(new Error('Not String'))
      }
    }

    resolve(words)
  })
}

// sort strings to alphabetical order
const sortWords = (capitalizeWords) => {
  return new Promise((resolve, reject) => {
    resolve(capitalizeWords.sort())
  })
}

// input string array 
makeAllCaps(['xyz', 'qwe', 'abc'])
  .then(capitalizeWords => sortWords(capitalizeWords))
  .then(sortedWords => { console.log(`Sorted Words: ${sortedWords}`) }) // output sorted array
  .catch(err => console.log(err))

/*
// For two seperate catch handlers
makeAllCaps(['xyz', 'qwe', 'abc'])
.then(capitalizeWords => sortWords(capitalizeWords)
  .then(sortedWords => { console.log(`Sorted Words: ${sortedWords}`) })
  .catch(err => console.log(err))
)
.catch(err => console.log(err))
*/

